// File:         Command.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Das Interface Command wird zum steuern des Roboters verwendet.
 */
interface Command {

	/**
	 * In der Methode execute wird der explizite Befehl ausgeführt.
	 */
	void execute();

}
