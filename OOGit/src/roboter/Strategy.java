// File:         Strategy.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Das Interface Strategy wird zum spezifizieren verschiedener Strategien verwendet um leicht zwischen Ihnen zu wechseln.
 */
interface Strategy {

	/**
	 * F�hrt die jeweilige Strategie aus.
	 */
	void operate();

}
