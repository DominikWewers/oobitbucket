// File:         ObserverThreat.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LightSensor;
import lejos.nxt.UltrasonicSensor;

/**
 * Die Klasse ObserverThread l�uft in einem eigenen Thread und liest in einer
 * while-schleife die Werte der Sensoren aus, welche dann an die jeweiligen
 * Observer �bermittelt werden.
 */
class ObserverThread extends Thread {

	/** Die Instanz der Klasse Robot. */
	private Roboter robot;

	/** Der Ultraschallsensor. */
	private UltrasonicSensor us;

	/** Der Lichtsensor. */
	private LightSensor ls;

	/** Der Ultraschall-Listener. */
	private SensorListener ultrasonicListener;

	/** Der Licht-Listener. */
	private SensorListener lightListener;

	/** Der Ultraschall-Observer. */
	private Observer ultrasonicObserver;

	/** Der Licht-Observer. */
	private Observer lightObserver;

	/**
	 * Instanziiert die Klasse ObserverThread.
	 */
	ObserverThread() {
		robot = Roboter.getInstance();
		us = robot.getUs();
		ls = robot.getLs();
		ultrasonicListener = robot.getUltrasonicListener();
		lightListener = robot.getLightListener();
		ultrasonicObserver = robot.getUltrasonicObserver();
		lightObserver = robot.getLightObserver();
	}

	/**
	 * Die Funktion run wird von der Klasse Thread geerbt und wird in dieser
	 * Implementation zum auslesen der Sensoren verwendet.
	 */
	public void run() {

		ultrasonicListener.register(ultrasonicObserver);
		lightListener.register(lightObserver);
		while (true) {
			ultrasonicListener.setState(us.getDistance());
			lightListener.setState(ls.getNormalizedLightValue());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
