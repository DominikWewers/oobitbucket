// File:         CubicStrategy.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Die Strategie CubicStrategy folgt der Linie mit Hilfe eine kubischen Funktion.
 */
class CubicStrategy implements Strategy {

	/** Die Instanz der Roboter Klasse. */
	private Roboter robot;

	/**
	 * Instanziiert die Klasse CubicStrategie.
	 */
	CubicStrategy() {
		super();
		robot = Roboter.getInstance();
	}

	/**
	 * Die Funktion operate wird von dem Interface Strategy geerbt 
	 * und wird in dieser Implementation f�r das Folgen einer Linie verwendet.
	 */
	public void operate() {

		while (robot.getUltrasonic() > 25 && robot.getCalibratedLight() < 25) {

			robot.setDirection((Math.pow((robot.getCalibratedLight() - 62.5), 3) / 527.34375));
			robot.getDriveForward().execute();

		}

	}

}
