// File:         DriveForward.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.NXTMotor;

/**
 * Das Kommando DriveForward realisiert das forw�rts fahren des Robots.
 */
class DriveForward implements Command {

	/** Die Motoren des Robots. */
	private NXTMotor left, right;

	/** Die Fahrtrichtung des Robots. */
	private double direction;

	/** Die Soll-Geschwindigkeit des Robots. */
	private double speed;

	/** Die Instanz der Klasse Robot. */
	private Roboter robot;

	/**
	 * Instanziiert die Klasse DriveForward.
	 */
	DriveForward() {
		super();
		robot = Roboter.getInstance();
		left = robot.getLeft();
		right = robot.getRight();
		direction = robot.getDirection();
		speed = robot.getSpeed();
	}

	/**
	 * Die Funktion execute wird von dem Interface Command geerbt 
	 * und wird in dieser Implementation f�r das forw�rts fahren des Robots verwendet.
	 */
	@Override
	public void execute() {
		direction = robot.getDirection();
		speed = robot.getSpeed();
		if (direction <= (-50)) {
			left.backward();
			right.forward();
			right.setPower((int) ((100) * speed));
			left.setPower((int) (((-2) * direction - 100) * speed));
		} else if (direction < 0) {
			left.forward();
			right.forward();
			right.setPower((int) ((100) * speed));
			left.setPower((int) ((2 * direction + 100) * speed));
		} else if (direction == 0) {
			left.forward();
			right.forward();
			right.setPower((int) (100 * speed));
			left.setPower((int) (100 * speed));
		} else if (direction <= 50) {
			left.forward();
			right.forward();
			right.setPower((int) (((-2) * direction + 100) * speed));
			left.setPower((int) (100 * speed));
		} else if (direction <= 100) {
			left.forward();
			right.backward();
			right.setPower((int) (((2) * direction - 100) * speed));
			left.setPower((int) (100 * speed));
		}

	}

}
