// File:         LightObserver.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Die Klasse LightObserver speichert einen neuen Lichtwert, wenn sie aufgerufen wird.
 */
class LightObserver implements Observer {

	/**
	 * Instanziiert die Klasse LightObserver.
	 */
	LightObserver() {
	};

	/** Die Instanz der Klasse Roboter. */
	private Roboter robot = Roboter.getInstance();

	/**
	 * Die Funktion update wird von dem Interface Observer geerbt 
	 * und wird in dieser Implementation aktualisieren der Lichtwerte verwendet.
	 * @param state Der neue Lichtwert
	 */
	public void update(int state) {

		robot.setCalibratedLight(state);

	}

}
