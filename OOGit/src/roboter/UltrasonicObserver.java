// File:         UltrasonicObserver.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Die Klasse UltrasonicObserver speichert einen neuen Ultraschallwert, wenn sie aufgerufen wird.
 */
class UltrasonicObserver implements Observer {

	/**
	 * Instanziiert die Klasse UltrasonicObserver.
	 */
	UltrasonicObserver() {
	};
	
	/** Die Instanz der Klasse Roboter. */
	private Roboter robot = Roboter.getInstance();

	/**
	 * Die Funktion update wird von dem Interface Observer geerbt 
	 * und wird in dieser Implementation aktualisieren der Ultraschallwerte verwendet.
	 * 
	 * @param state der neue Ultraschallwert.
	 */
	public void update(int state) {

		robot.setUltrasonic(state);

	}

}
