// File:         SearchLine.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;

/**
 * Der Status SearchLine versucht die Linie zu finden, indem er den Roboter eine Spirale fahren l�sst.
 * Wenn er die Linie findet wechselt er in den Status FollowLine.
 */
class SearchLine implements State {

	/** Die Instanz der Klasse Roboter. */
	private Roboter robot = Roboter.getInstance();

	/** Z�hlvariable. */
	private int i = 5;

	/**
	 * Die Funktion operate wird von dem Interface State geerbt 
	 * und wird in dieser Implementation zum Suchen der Linie verwendet.
	 */
	@Override
	public void operate() {

		LCD.drawString("Search Line", 1, 1);

		while (i < 95 && robot.getCalibratedLight() > 55) {
			if (i < 95) {
				i++;
			}
			// Pause for i*100 milliseconds
			try {
				Thread.sleep(i * 100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Adjust Drive

			robot.setDirection(i);
			robot.getDriveForward().execute();

		}

		LCD.clear();

		robot.setState(new FollowLine());
		robot.operate();

	}

}
