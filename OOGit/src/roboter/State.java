// File:         State.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Das Interface State wird zum spezifizieren verschiedener Status verwendet um leicht Statusübergänge zu implementieren.
 */
interface State {

	/**
	 * Führt den jeweiligen Status aus.
	 */
	void operate();
}