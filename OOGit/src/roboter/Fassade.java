// File:         Fassade.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

 // TODO: Auto-generated Javadoc
/**
  * Die Fassade realisiert die Schnittstelle des Packets roboter zu anderen Paketen 
  * und wird zu steuern des Robots verwendet.
  */
 public class Fassade {

	/** Die Instanz der Klasse Fassade. */
	private static Fassade instance;
	
	/** Die Instanz der Klasse Robot. */
	private Roboter robot;

	/**
	 *	Instanziiert die Klasse Fassade.
	 */
	private Fassade() {
		robot = Roboter.getInstance();
	}

	/**
	 *	Startet den Thread der Klasse Robot.
	 */
	public void startRobot() {
		robot.setDaemon(true);
		robot.start();
	}

	/**
	 * Stoppt den Thread der Klasse Robot.
	 */
	public void stopRobot() {
		robot.interrupt();
	}

	/**
	 * Stellt die Geschwindigkeit des Robots ein.
	 *
	 * @param speed the new speed
	 */
	public void setSpeed(double speed) {
		robot.setSpeed(speed);
	}

	/**
	 * Gibt die einzige Instanz der Klasse Fassade zur�ck.
	 *
	 * @return einzige Instanz der Klasse Fassade.
	 */
	public static Fassade getInstance() {
		if (instance == null) {
			instance = new Fassade();
		}
		return instance;
	}

}
