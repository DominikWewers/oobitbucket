// File:         Observer.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

/**
 * Das Interface Observer wird zu überwachen der Sensoren verwendet.
 */
interface Observer {

	/**
	 * Aktualisiert die Werte der Sensoren.
	 *
	 * @param state Der neue Wert des Sensors.
	 */
	void update(int state);

}
