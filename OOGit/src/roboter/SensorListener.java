// File:         SensorListener.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

// TODO: Auto-generated Javadoc
/**
 *	Die Klasse SensorListener empfängt Sensorwerte und leitet sie an alle Observer weiter.
 */
class SensorListener extends Subject {
	
	/**
	 * Leitet den übergebenen Wert an alle Observer weiter.
	 *
	 * @param pState der aktuelle Sensorwert.
	 */
	void setState(int pState) {
		notifyObservers(pState);
	}

}
