// File:         PIDStrategy.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;

/**
 * Die Strategie PIDStrategy folgt der Linie basierend auf einem PID-Regler.
 */
class PIDStrategy implements Strategy {

	/** Die Instanz der Roboter Klasse. */
	private Roboter robot;

	/** Variablen zur Berechnung der neuen Fahrtrichtung. */
	private double correctiondivisor = 25, mincorrection = -1, maxcorrection = 1, midpoint = 25, kp = (float) 0.9,
			ki = (float) 0.003, kd = (float) 0.5, lasterror = 0, value = 50, error = 0, integral = 0, derivative = 0,
			correction = 0;
	
	/** Z�hlvariable. */
	private int i = 1000;

	/**
	 * Instanziiert die Klasse PIDStrategie.
	 */
	PIDStrategy() {
		super();
		robot = Roboter.getInstance();
	}
	
	/**
	 * Die Funktion operate wird von dem Interface Strategy geerbt 
	 * und wird in dieser Implementation f�r das Folgen einer Linie verwendet.
	 */
	public void operate() {
		//Lichtwerte und Ultraschallwerte �berpr�fen
		while (i > 0) {
			i--;
			while (robot.getUltrasonic() > 25 && robot.getCalibratedLight() < 50) {
				i = 1000;
				value = robot.getCalibratedLight();

				if (value > 50) {
					error = midpoint - 50;
				} else {
					error = midpoint - value;
				}

				integral = error + integral;
				derivative = error - lasterror;
				correction = kp * error + ki * integral + kd * derivative;

				if (correction > maxcorrection) {
					maxcorrection = correction;
					if (maxcorrection > correctiondivisor) {
						correctiondivisor = maxcorrection;
					}
				}

				if (correction < mincorrection) {
					mincorrection = correction;
					if ((-mincorrection) > correctiondivisor) {
						correctiondivisor = (-mincorrection);
					}
				}

				LCD.drawInt((int) (correction / correctiondivisor * 100), 1, 3);

				robot.setDirection((correction / correctiondivisor * 100));
				robot.getDriveForward().execute();

				lasterror = error;

			}
		}

		robot.setState(new Avoid());
		robot.operate();

	}

}
