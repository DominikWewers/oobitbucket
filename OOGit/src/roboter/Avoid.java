// File:         Avoid.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;

/**
 * Der Status Avoid versucht ein Hindernis zu umfahren.
 * Falls w�hrend dessen erneut ein Hindernis auftaucht, wird Avoid neu gestartet.
 * Wenn die Linie wiedergefunden wird zum Status FollowLine gewechselt 
 */
class Avoid implements State {

	/** Die Instanz der Roboter Klasse. */
	private Roboter robot;

	/** Die Geschwindigkeit die vor Avoid verwendet wurde. */
	private double oldSpeed;

	/**	Wirde verwendet um die Statuswechsel zu realisieren. */
	private boolean on = true;

	/**
	 * Instanziiert die Klasse Avoid.
	 */
	Avoid() {
		super();
		robot = Roboter.getInstance();
	}
	
	/**
	 * Die Funktion operate wird von dem Interface State geerbt 
	 * und wird in dieser Implementation f�r das Ausweichen verwendet.
	 */
	@Override
	public void operate() {
		LCD.drawString("Avoid", 1, 1);

		oldSpeed = robot.getSpeed();

		robot.setSpeed(1);

		robot.setDirection(-25);
		robot.getDriveForward().execute();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		robot.setDirection(25);
		robot.getDriveForward().execute();

		while (on) {
			if (robot.getUltrasonic() > 25) {
				on = false;
				robot.setState(new Avoid());
			}
			if (robot.getCalibratedLight() > 50) {
				on = false;
				robot.setState(new FollowLine());
			}
		}
		robot.setSpeed(oldSpeed);

		LCD.clear();

		robot.operate();

	}

}
