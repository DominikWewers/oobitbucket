// File:         FollowLine.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;

/**
 * Der Status FollowLine versucht mittels einer zuvor spezifizierten Strategie einer Linie zu folgen.
 */
class FollowLine implements State {

	/** Die Instanz der Klasse Roboter. */
	private Roboter robot = Roboter.getInstance();

	/**
	 * Die Funktion operate wird von dem Interface State geerbt 
	 * und wird in dieser Implementation Folgen der Linie verwendet.
	 */
	@Override
	public void operate() {

		LCD.drawString("Follow Line", 1, 1);

		robot.getStrat().operate();

		LCD.clear();

		robot.setState(new Avoid());
		robot.operate();

	}

}
