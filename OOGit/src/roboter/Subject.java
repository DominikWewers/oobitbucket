// File:         Subject.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import java.util.ArrayList;
import java.util.List;

/**
 *	Die Klasse Subject registriert und entfernt observer, die �ber einen Listener informiert werden.
 */
abstract class Subject {

	/** Die Liste der zu informierenden Observer. */
	private List<Observer> observerList = new ArrayList<Observer>();

	/**
	 * Registrieren eines Observers.
	 *
	 * @param pNewObserver der zu registrierende Observer
	 */
	void register(Observer pNewObserver) {
		observerList.add(pNewObserver);
	}

	/**
	 * Entfernen eines Observers.
	 *
	 * @param pNewObserver der zu entfernende Observer.
	 */
	void unregister(Observer pNewObserver) {
		observerList.remove(pNewObserver);
	}

	/**
	 * Informiere alle Observer.
	 *
	 * @param pState die Information.
	 */
	protected void notifyObservers(int pState) {
		for (Observer observer : observerList) {
			observer.update(pState);
		}
	}
}
