// File:         Shutdown.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;

// TODO: Auto-generated Javadoc
/**
 * Der Status Shutdown beendet das Programm.
 */
class Shutdown implements State {

	/**
	 * Die Funktion operate wird von dem Interface State geerbt 
	 * und wird in dieser Implementation zum abschalten des Programms verwendet.
	 */
	@Override
	public void operate() {

		LCD.drawString("Shutdown", 1, 1);

		LCD.clear();

	}

}
