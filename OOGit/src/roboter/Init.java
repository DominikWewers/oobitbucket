// File:         Init.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.Button;
import lejos.nxt.LCD;

// TODO: Auto-generated Javadoc
/**
 * Der Status warten auf einen Tastendruck um dann den Roboter zu initialisieren und in den Status Folge Linie zu wechseln.
 */
class Init implements State {

	/** Die Instanz der Roboter Klasse. */
	private Roboter robot = Roboter.getInstance();

	/**
	 * Die Funktion operate wird von dem Interface State geerbt 
	 * und wird in dieser Implementation zum Initialisieren des Robots verwendet.
	 */
	@SuppressWarnings("static-access")
	@Override
	public void operate() {

		LCD.drawString("Press any Button", 1, 1);

		Button.ENTER.waitForAnyPress();

		robot.getObserver().setDaemon(true);
		robot.getObserver().start();
		robot.setState(new FollowLine());
		robot.operate();

		LCD.clear();

	}

}
