// File:         Roboter.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package roboter;

import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

/**
 * Die Klasse Roboter h�lt alle Instanzen und Variablen die ben�tigt werden und
 * bietet entsprechende getter und setter an.
 */
class Roboter extends Thread {

	/** Die Strategie die verwendet werden soll. */
	private Strategy strat;

	/** Der linke Motor. */
	private NXTMotor left = new NXTMotor(MotorPort.A);

	/** Der rechte Motor. */
	private NXTMotor right = new NXTMotor(MotorPort.B);

	/** Das Kommando DriveForward. */
	private Command driveForward = new DriveForward();

	/** Der Observer Thread. */
	private ObserverThread observer = new ObserverThread();

	/** Der kalibrierte Lichtwert. */
	private double calibratedLight = 50;

	/** Lichtwerte zur bestimmung des kalibrierten Lichtwertes. */
	private int minLight = 510, maxLight = 512, rawLight = 511;
	
	/** Die Fahrtrichtung. */
	private double direction = 0;
	
	/** Der Ultraschallwert. */
	private int ultrasonic = 127;

	/** Der Status in dem der Roboter sich befindet. */
	private State state;

	/** Die Instanz der Robot Klasse. */
	private static Roboter instance;

	/** Die Geschwindigkeit in Prozent vom Maximum. */
	private double speed = 0.4;

	/** Der Ultraschallsensor. */
	private UltrasonicSensor us = new UltrasonicSensor(SensorPort.S2);
	
	/** Der Lichtsensor. */
	private LightSensor ls = new LightSensor(SensorPort.S1);

	/** Der Ultraschall-Listener. */
	private SensorListener ultrasonicListener = new SensorListener();
	
	/** Der Licht-Listener. */
	private SensorListener lightListener = new SensorListener();

	/** Der Ultraschall-Observer. */
	private Observer ultrasonicObserver = new UltrasonicObserver();
	
	/** Der Licht-Observer. */
	private Observer lightObserver = new LightObserver();
	
	/**
	 * Instanziiert die Klasse Roboter.
	 */
	private Roboter() {
		strat = new PIDStrategy();
	}

	/**
	 * Gibt Ultraschall Sensor zur�ck.
	 *
	 * @return der Ultraschall Sensor.
	 */
	 UltrasonicSensor getUs() {
		return us;
	}
	
	/**
	 * Gibt den Licht Sensor zur�ck.
	 *
	 * @return der Licht Sensor.
	 */
	 LightSensor getLs() {
		return ls;
	}
	
	/**
	 * Gibt den Ulraschall Listener zur�ck.
	 *
	 * @return der Ultraschall Listener.
	 */
	 SensorListener getUltrasonicListener() {
		return ultrasonicListener;
	}
	
	/**
	 * Gibt Licht Listener zur�ck.
	 *
	 * @return der Licht Listener.
	 */
	 SensorListener getLightListener() {
		return lightListener;
	}
	
	/**
	 * Gibt den Ultraschall Observer zur�ck.
	 *
	 * @return der Ultraschall Observer.
	 */
	 Observer getUltrasonicObserver() {
		return ultrasonicObserver;
	}

	/**
	 * Gibt den Licht Observer zur�ck.
	 *
	 * @return der Licht Observer.
	 */	
	 Observer getLightObserver() {
		return lightObserver;
	}

	/**
	 * Gibt das Kommando zum vorw�rts fahren zur�ck.
	 *
	 * @return das Kommando zum vorw�rts fahren.
	 */
	Command getDriveForward() {
		return driveForward;
	}

	/**
	 * Gibt Fahrtrichtung zur�ck.
	 *
	 * @return die Fahrtrichtung
	 */
	double getDirection() {
		return direction;
	}

	/**
	 * Setzt die Fahrtrichtung.
	 *
	 * @param newDirection die neue Fahrtrichtung
	 */
	void setDirection(double newDirection) {
		direction = newDirection;
	}

	/**
	 * Gibt den Observer Thread zur�ck.
	 *
	 * @return Der Observer thread.
	 */
	ObserverThread getObserver() {
		return observer;
	}

	/**
	 * Gibt die Geschwindigkeit zur�ck.
	 *
	 * @return Die Geschwindigkeit
	 */
	double getSpeed() {
		return speed;
	}

	/**
	 * Setzt die Geschwindigkeit.
	 *
	 * @param newSpeed die neue Geschwindigkeit
	 */
	void setSpeed(double newSpeed) {
		speed = newSpeed;
	}

	/**
	 * Gibt den aktuellen Status zur�ck.
	 *
	 * @return der aktuelle Status
	 */
	State getState() {
		return state;
	}

	/**
	 * Setzt den aktuellen Status.
	 *
	 * @param newState der neue Status.
	 */
	void setState(State newState) {
		state = newState;
	}

	/**
	 * Gibt die Strategie zur�ck.
	 *
	 * @return die Strategie.
	 */
	Strategy getStrat() {
		return strat;
	}

	/**
	 * Gibt den linken Motor zur�ck.
	 *
	 * @return der linke Motor.
	 */
	NXTMotor getLeft() {
		return left;
	}

	/**
	 * Gibt den rechtenMotor zur�ck.
	 *
	 * @return Der rechte Motor.
	 */
	NXTMotor getRight() {
		return right;
	}

	/**
	 * F�hrt den aktuellen Status aus.
	 */
	void operate() {
		if (state != null) {
			state.operate();
		}
	}

	/**
	 * Gibt den kalibrierten Lichtwert zur�ck.
	 *
	 * @return der kalibrierte Lichtwert.
	 */
	double getCalibratedLight() {
		return calibratedLight;
	}

	/**
	 * Setzt die Lichtwerte.
	 *
	 * @param newRawLight der neue raw Lichtwert.
	 */
	void setCalibratedLight(int newRawLight) {
		rawLight = newRawLight;
		if (minLight > newRawLight) {
			minLight = newRawLight;
		}
		if (maxLight < newRawLight) {
			maxLight = newRawLight;
		}

		calibratedLight = (((float) rawLight - (float) minLight) * 100 / ((float) maxLight - (float) minLight));
		LCD.drawInt(minLight, 4, 1);
	}

	/**
	 * Gibt den Ultraschallwert zur�ck.
	 *
	 * @return der Ultraschallwert.
	 */
	int getUltrasonic() {
		return ultrasonic;
	}

	/**
	 * Setzt den Ultraschallwert.
	 *
	 * @param newUltrasonic der neue Ultraschallwert.
	 */
	void setUltrasonic(int newUltrasonic) {
		ultrasonic = newUltrasonic;
	}

	/**
	 * Gibt die einzige Instanz der Roboter Klasse zur�ck.
	 *
	 * @return Die einzige Instanz der Roboter Klasse.
	 */
	static Roboter getInstance() {
		if (instance == null) {
			instance = new Roboter();
		}
		return instance;
	}

	/**
	 * Setzt den Status des Roboters auf Init und f�hrt diesen aus.
	 */
	public void run() {
		state = new Init();
		operate();
	}
}
