// File:         Main.java
// Created:      10.06.2016
// Last Changed: 11.06.2016
// Author:       Dominik Wewers
package main;

import lejos.nxt.Button;
import roboter.Fassade;

/**
 * Die auszuführende Klasse Main.
 */
public class Main {
	
	/**
	 * Der private Konstruktor der Klasse Main.
	 */
	private Main() {

	}

	/** Die Fassade um den Roboter zu steuern. */
	private static Fassade roboter;

	/**
	 * Die auszuführende main Methode.
	 * Holt sich die Instanz der Fassade, 
	 * und lässt den Roboter laufen bis eine beliebige Taste gedückt wird.
	 */
	@SuppressWarnings("static-access")
	public static void main() {
		roboter = Fassade.getInstance();
		roboter.startRobot();
		Button.ESCAPE.waitForAnyPress();
		roboter.stopRobot();
	}

}
